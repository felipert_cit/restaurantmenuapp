package com.felipe.firebase;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.felipe.firebase.entity.Meal;
import com.felipe.firebase.entity.Restaurant;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.MutableData;
import com.firebase.client.Transaction;
import com.firebase.client.ValueEventListener;
import com.firebase.ui.FirebaseListAdapter;

import java.io.ByteArrayOutputStream;

public class MenuActivity extends AppCompatActivity {

    private ListView listMenu;
    private  int idRestaurant;
    private TextView txtNameRestaurant;
    private Button btnAdd;
    private EditText editName;
    private EditText editDescription;
    private String nameRestaurant;

    private static final int CAMERA_REQUEST = 1888;
    private ImageView imageView;
    private Bitmap photo;

    Firebase ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        if(getIntent().hasExtra("restaurantID")){
            Toast.makeText(MenuActivity.this,String.valueOf( getIntent().getExtras().getInt("restaurantID")), Toast.LENGTH_LONG).show();
            idRestaurant = getIntent().getExtras().getInt("restaurantID");

            listMenu = (ListView) findViewById(R.id.menu_list_food);
            txtNameRestaurant = (TextView) findViewById(R.id.menu_label_name);
            btnAdd = (Button) findViewById(R.id.menu_button_add);
            editName = (EditText) findViewById(R.id.menu_edit_name);
            editDescription = (EditText) findViewById(R.id.menu_edit_description);

            imageView = (ImageView) findViewById(R.id.menu_image);

            Button btnAddimage = (Button) findViewById(R.id.menu_button_addimage);

            btnAddimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            });

            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                      ref.runTransaction(new Transaction.Handler() {
                          @Override
                          public Transaction.Result doTransaction(MutableData mutableData) {
                              Log.e("info_mut_data_initial", mutableData.getValue().toString());

                            final  Restaurant r = mutableData.getValue(Restaurant.class);

                              Meal meal = new Meal(r.getMenu().size(), editName.getText().toString(), editDescription.getText().toString());

                              if (photo != null) {

                                  ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                                  photo.compress(Bitmap.CompressFormat.PNG, 1, byteArrayOutputStream);

                                  byte[] byteArray = byteArrayOutputStream.toByteArray();

                                  String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

                                  meal.setImage(encoded);
                              }
                              r.addMenu(meal);

                              mutableData.setValue(r);

                              runOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {

                                      photo = null;
                                      editName.setText("");
                                      editDescription.setText("");
                                      imageView.setImageBitmap(null);

                                      Toast.makeText(MenuActivity.this, "Refeição "+ r.getName()+" adicionada!", Toast.LENGTH_SHORT).show();

                                  }
                              });

                              return Transaction.success(mutableData);
                          }

                          @Override
                          public void onComplete(FirebaseError firebaseError, boolean b, DataSnapshot dataSnapshot) {
                              if (firebaseError != null) {
                                  Log.e("on_complete_error", firebaseError.getMessage());
                              }
                              Log.e("on_complete", "error = " + firebaseError + " commited = " + b);

                          }
                      });

                }
            });

            ref = FirebaseClass.getFirebase().child("restaurants").child(String.valueOf(idRestaurant));


            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    nameRestaurant = dataSnapshot.getValue(Restaurant.class).getName();
                    txtNameRestaurant.setText("Restaurante " + nameRestaurant);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });


        FirebaseListAdapter<Meal> mAdapter = new FirebaseListAdapter<Meal>(MenuActivity.this, Meal.class, R.layout.layout_item_restaurant, ref.child("menu")) {
            @Override
            protected void populateView(View view, Meal meal) {
                TextView text = (TextView) view.findViewById(R.id.item_name);
                text.setText(meal.getName());

                view.setId(meal.getId());

                text = (TextView) view.findViewById(R.id.item_description);
                text.setText(meal.getDescription());

                Log.i("bindList",meal.toString());

                if(meal.getImage()!=null) {
                    ImageView image = (ImageView) view.findViewById(R.id.item_image);

                    byte[] decodedString = Base64.decode(meal.getImage(), Base64.DEFAULT);

                   final Bitmap decoded = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                    image.setImageBitmap(decoded);


                    image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog nagDialog = new Dialog(MenuActivity.this,android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                            nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            nagDialog.setCancelable(false);
                            nagDialog.setContentView(R.layout.preview_image);
                            ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
                            ivPreview.setImageBitmap(decoded);

                            ivPreview.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View arg0) {

                                    nagDialog.dismiss();
                                }
                            });

                            nagDialog.show();
                        }
                    });
                }
            }


        };
            listMenu.setAdapter(mAdapter);

            listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(MenuActivity.this, "idRestaurant= " + idRestaurant + " idMenu" + view.getId(), Toast.LENGTH_SHORT).show();

                    Intent intentMenu = new Intent(MenuActivity.this, IngredientActivity.class);

                    Bundle bundle = new Bundle();

                    bundle.putInt("restaurantID", idRestaurant);
                    bundle.putInt("menuID", view.getId());
                    bundle.putString("nameRestaurant", nameRestaurant);
                    intentMenu.putExtras(bundle);

                    startActivity(intentMenu);

                }
            });

        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            photo = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(photo);
        }
    }
}
