package com.felipe.firebase.entity;

/**
 * Created by fetav on 29/03/2016.
 */
public class Ingredient {

    private int id;
    private String name;
    private String description;
    private String quantity;

    public Ingredient(String name, String description, String quantity) {
        this.name = name;
        this.description = description;
        this.quantity = quantity;
    }

    public Ingredient(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
