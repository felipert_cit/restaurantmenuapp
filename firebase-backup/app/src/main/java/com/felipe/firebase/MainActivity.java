package com.felipe.firebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.felipe.firebase.entity.User;
import com.firebase.client.AuthData;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {


    private EditText textEmail;
    private EditText textPassword;
    private Button buttonCreate;
    private Button buttonLogin;

    private User user;
    Firebase firebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();
        firebase = FirebaseClass.getFirebase();

    }

    private void initComponents(){

        textEmail = (EditText) findViewById(R.id.main_text_email);
        textPassword = (EditText) findViewById(R.id.main_text_password);
        buttonCreate = (Button) findViewById(R.id.main_button_create);
        buttonLogin = (Button) findViewById(R.id.main_button_login);

        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initUser();
                saveUser();
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initUser();
                verifyLogin();
            }
        });

    }


    protected void initUser(){
        user = new User();
        user.setName( textEmail.getText().toString() );
        user.setEmail( textEmail.getText().toString() );
        user.setPassword(textPassword.getText().toString());

    }


    private void saveUser(){

        firebase.createUser(
                user.getEmail(),
                user.getPassword(),
                new Firebase.ValueResultHandler<Map<String, Object>>() {
                    @Override
                    public void onSuccess(Map<String, Object> stringObjectMap) {

                        user.setId(stringObjectMap.get("uid").toString());

                        Firebase firebase = FirebaseClass.getFirebase();
                        firebase = firebase.child("users").child(user.getId());

                        user.setPassword(null);
                        user.setId(null);
                        firebase.setValue(user);

                        firebase.unauth();

                        Toast.makeText(getApplicationContext(), "Conta criada com sucesso", Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onError(FirebaseError firebaseError) {
                        Toast.makeText(getApplicationContext(), firebaseError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    private void verifyLogin(){

        firebase.authWithPassword(
                user.getEmail(),
                user.getPassword(),
                new Firebase.AuthResultHandler() {
                    @Override
                    public void onAuthenticated(AuthData authData) {
                        Toast.makeText(getApplicationContext(), "OK !", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onAuthenticationError(FirebaseError firebaseError) {
                        Toast.makeText(getApplicationContext(), "!!! ERRO !!!", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }
}
