package com.felipe.firebase;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.felipe.firebase.entity.Restaurant;
import com.firebase.client.Firebase;
import com.firebase.ui.FirebaseListAdapter;


import java.util.Random;

public class RestaurantActivity extends AppCompatActivity {

    private EditText txtName;
    private EditText txtDescription;
    private Button btnAdd;
    private ListView listRestaurant;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        txtName = (EditText) findViewById(R.id.restaurant_text_name);
        txtDescription = (EditText) findViewById(R.id.restaurant_text_description);
        btnAdd = (Button) findViewById(R.id.restaurant_button_add);
        listRestaurant = (ListView) findViewById(R.id.restaurant_list);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addRestaurant();
            }
        });


        Firebase ref = FirebaseClass.getFirebase().child("restaurants");


        FirebaseListAdapter<Restaurant> mAdapter = new FirebaseListAdapter<Restaurant>(this, Restaurant.class, R.layout.layout_item_restaurant, ref) {
            @Override
            protected void populateView(View view, Restaurant object) {

                TextView text = (TextView) view.findViewById(R.id.item_name);
                text.setText(object.getName());

                view.setId(object.getId());

                text = (TextView) view.findViewById(R.id.item_description);
                text.setText(object.getDescription());

            }

        };
        listRestaurant.setAdapter(mAdapter);

        listRestaurant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(RestaurantActivity.this, "id= " + view.getId(), Toast.LENGTH_SHORT).show();

                Intent intentMenu = new Intent(RestaurantActivity.this,MenuActivity.class);

                Bundle bundle = new Bundle();

                bundle.putInt("restaurantID", view.getId());
                intentMenu.putExtras(bundle);

                startActivity(intentMenu);

            }
        });

        listRestaurant.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final int idv = view.getId();

                new AlertDialog.Builder(RestaurantActivity.this)
                        .setTitle("Apagar")
                        .setMessage("Deseja realmente apagar este restaurante?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                                Firebase ref = FirebaseClass.getFirebase().child("restaurants").child(String.valueOf(idv));

                                ref.removeValue();

                                Toast.makeText(RestaurantActivity.this, "Apagado com sucesso! ", Toast.LENGTH_SHORT).show();

                            }})
                        .setNegativeButton(android.R.string.no, null).show();



                return true;
            }
        });

    }

    private int getRandom(int min,int max){

        Random rand = new Random();

        return Math.abs(rand.nextInt((max - min) + 1) + min);
    }

    private void addRestaurant() {
        Restaurant re = new Restaurant();

        re.setId(getRandom(1, 1000));
        re.setName(txtName.getText().toString());
        re.setDescription(txtDescription.getText().toString());


        Firebase firebase = FirebaseClass.getFirebase();

        firebase = firebase.child("restaurants").child(String.valueOf(re.getId()));

        // estudar como funciona o push...
        firebase.setValue(re);

        txtDescription.setText("");
        txtName.setText("");

        Toast.makeText(RestaurantActivity.this, "Restaurante "+ re.getName()+" adicionado com sucesso!", Toast.LENGTH_SHORT).show();
    }
}
