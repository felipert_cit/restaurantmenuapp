package com.felipe.firebase.entity;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Meal {
    private int id;
    private String name;
    private String description;
    private List<Ingredient> ingredients;
    private String image;

    public Meal(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        ingredients = new ArrayList<Ingredient>();
    }



    public Meal(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addIngredient(String name,String value){

        if (this.ingredients == null)
            this.ingredients = new ArrayList<Ingredient>();

        this.ingredients.add(new Ingredient(name,description,""));

    }

    public void addIngredient(Ingredient ing){

        if (this.ingredients == null)
            this.ingredients = new ArrayList<Ingredient>();

        this.ingredients.add(ing);

    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Ingredient> getIngredients() {

        if(this.ingredients ==null)
        this.ingredients = new ArrayList<Ingredient>();

        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {

        String ingred = "{";

        for (Ingredient ing : this.getIngredients() ) {

            ingred.concat(ing.getName()).concat(",");
        }
        ingred.concat("}");

        return "Dish{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", ingredients= " + ingred +
                '}';
    }
}
