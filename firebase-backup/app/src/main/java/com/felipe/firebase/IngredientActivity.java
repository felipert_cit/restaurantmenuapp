package com.felipe.firebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.felipe.firebase.entity.Meal;
import com.felipe.firebase.entity.Ingredient;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.MutableData;
import com.firebase.client.Transaction;
import com.firebase.client.ValueEventListener;
import com.firebase.ui.FirebaseListAdapter;

public class IngredientActivity extends AppCompatActivity {

    int idRestaurant;
    int idMenu;
    String nameRestaurant;

    TextView textTitle;
    EditText textName;
    EditText textDescription;
    EditText textQuantity;


    ListView list;
    Button btnAdd;

    Firebase ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredient);

        if(getIntent().hasExtra("restaurantID")){
            Toast.makeText(this, String.valueOf(getIntent().getExtras().getInt("restaurantID")), Toast.LENGTH_LONG).show();
            idRestaurant = getIntent().getExtras().getInt("restaurantID");
            nameRestaurant = getIntent().getExtras().getString("nameRestaurant");
            idMenu = getIntent().getExtras().getInt("menuID");

            textTitle = (TextView) findViewById(R.id.ingredient_title);
            textName = (EditText) findViewById(R.id.ingredient_text_name);
            textDescription = (EditText) findViewById(R.id.ingredient_text_description);
            textQuantity = (EditText) findViewById(R.id.ingredient_text_quantity);


            list = (ListView) findViewById(R.id.ingredient_list);

            ref = FirebaseClass.getFirebase()
                               .child("restaurants")
                               .child(String.valueOf(idRestaurant))
                               .child("menu")
                               .child(String.valueOf(idMenu));

            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    textTitle.setText(nameRestaurant + "\nRefeição: " + dataSnapshot.getValue(Meal.class).getName());
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });


            btnAdd = (Button) findViewById(R.id.ingredient_button_add);

            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ref.runTransaction(new Transaction.Handler() {
                        @Override
                        public Transaction.Result doTransaction(MutableData mutableData) {

                          Log.i("valor = ",mutableData.getKey()+" > "+mutableData.getValue());

                            Meal meal = mutableData.getValue(Meal.class);

                           final Ingredient ingredient = new Ingredient();

                            ingredient.setId(meal.getIngredients().size());

                            ingredient.setName(textName.getText().toString());
                            ingredient.setDescription(textDescription.getText().toString());
                            ingredient.setQuantity(textQuantity.getText().toString());

                            meal.addIngredient(ingredient);

                            mutableData.setValue(meal);



                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    textName.setText("");
                                    textDescription.setText("");
                                    textQuantity.setText("");

                                    Toast.makeText(IngredientActivity.this, "Ingrediente " + ingredient.getName() + " adicionado!", Toast.LENGTH_SHORT).show();

                                }
                            });



                            return Transaction.success(mutableData);
                        }

                        @Override
                        public void onComplete(FirebaseError firebaseError, boolean b, DataSnapshot dataSnapshot) {
                            if (firebaseError != null) {
                                Log.e("on_complete_error", firebaseError.getMessage());
                            }
                     //       Log.e("on_complete", dataSnapshot.toString() + " commited = " + b);

                        }
                    });

                }
            });

            FirebaseListAdapter<Ingredient> mAdapter = new FirebaseListAdapter<Ingredient>(this, Ingredient.class, android.R.layout.two_line_list_item, ref.child("ingredients")) {
                @Override
                protected void populateView(View view, Ingredient ingredient) {
                    TextView text = (TextView) view.findViewById(android.R.id.text1);
                    text.setText(ingredient.getName());

                    view.setId(ingredient.getId());

                    text = (TextView) view.findViewById(android.R.id.text2);
                    text.setText(ingredient.getDescription());
                }


            };
            list.setAdapter(mAdapter);
        }

        }
}
