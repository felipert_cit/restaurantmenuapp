package com.cit.menurestaurant.ui;

import android.content.ContentProvider;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.cit.menurestaurant.R;
import com.cit.menurestaurant.business.UserBusiness;
import com.cit.menurestaurant.entity.Restaurant;
import com.cit.menurestaurant.entity.User;
import com.cit.menurestaurant.infrastructure.FirebaseClass;
import com.cit.menurestaurant.infrastructure.OperationListener;
import com.cit.menurestaurant.infrastructure.OperationResult;
import com.cit.menurestaurant.manager.UserManager;
import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class LoginActivity extends AppCompatActivity {

    private Button buttonRegister;
    private Button buttonLogin;
    private CheckBox checkRemember;
    private EditText textLogin;
    private EditText textPassword;

    private SharedPreferences mSharedPreferences;



    private UserManager userManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        validRememberUser();


        buttonRegister = (Button) findViewById(R.id.login_button_create);
        buttonLogin = (Button) findViewById(R.id.login_button_login);
        checkRemember =  (CheckBox) findViewById(R.id.login_check_remember_login);
        textLogin = (EditText) findViewById(R.id.login_text_login);
        textPassword = (EditText) findViewById(R.id.login_text_password);

        userManager = new UserManager(this);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               login();
            }
        });

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
    }

    private void login() {

        final Firebase firebase = FirebaseClass.getFirebase();

        firebase.authWithPassword(
                textLogin.getText().toString(),
                textPassword.getText().toString(),
                new Firebase.AuthResultHandler() {
                    @Override
                    public void onAuthenticated(AuthData authData) {


                        Log.i("authData",authData.toString());

                        String successMessage = "Login successfully!\n Welcome ";

                        Toast.makeText(LoginActivity.this, successMessage, Toast.LENGTH_LONG).show();

                        final SharedPreferences.Editor editor = mSharedPreferences.edit();

                        editor.putString(UserBusiness.KEY_USER_LOGIN, authData.getUid());
                        editor.apply();

                        firebase.getRoot().child("users").child(authData.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                try {
                                    editor.putString(UserBusiness.KEY_USER_NAME,dataSnapshot.getValue(User.class).getName());
                                    editor.apply();
                                }catch (Exception ex){
                                }
                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {

                            }
                        });



                        if (checkRemember.isChecked()) {
                            editor.putBoolean(UserBusiness.KEY_USER_REMEMBER, true);
                            editor.apply();
                        }

                        startActivity(new Intent(LoginActivity.this, RestaurantActivity.class));
                        finish();
                    }

                    @Override
                    public void onAuthenticationError(FirebaseError firebaseError) {
                        Toast.makeText(getApplicationContext(), firebaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    private void validRememberUser() {

        Intent intent = getIntent();

        if (intent.getExtras() !=null)
            if(intent.getExtras().getBoolean("logout",false)){

                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.remove(UserBusiness.KEY_USER_REMEMBER);
                editor.apply();
            }

        Boolean userLogin = mSharedPreferences.getBoolean(UserBusiness.KEY_USER_REMEMBER, false);

        if (userLogin){
            startActivity(new Intent(LoginActivity.this, RestaurantActivity.class));
        }

    }





 }
