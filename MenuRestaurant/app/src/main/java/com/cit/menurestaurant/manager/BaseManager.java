package com.cit.menurestaurant.manager;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fetav on 29/02/2016.
 */
public class BaseManager {
    private List<AsyncTask<?, ?, ?>> mTaskList;
    protected Context mContext;

    protected BaseManager(Context context) {
        mContext = context;
        mTaskList = new ArrayList<AsyncTask<?, ?, ?>>();
    }

    public void cancelOperations() {
        for (AsyncTask<?, ?, ?> task : mTaskList) {
            task.cancel(false);
        }
    }

    protected void addToTaskList(AsyncTask<?, ?, ?> task) {
        mTaskList.add(task);
    }

    protected void removeFromTaskList(AsyncTask<?, ?, ?> task) {
        mTaskList.remove(task);
    }
}
