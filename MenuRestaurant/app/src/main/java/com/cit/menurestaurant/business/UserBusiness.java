package com.cit.menurestaurant.business;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.cit.menurestaurant.entity.User;
import com.cit.menurestaurant.infrastructure.FirebaseClass;
import com.cit.menurestaurant.infrastructure.OperationResult;
import com.cit.menurestaurant.repository.database.Database;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import org.json.JSONObject;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by felipert on 15/02/2016.
 */
public class UserBusiness extends BaseBusiness {

   public static final String KEY_USER_LOGIN = "login";
    public static final String KEY_USER_REMEMBER = "remember";
    public static final String KEY_USER_NAME = "name";

   int error;
    public UserBusiness(Context context) {
        super(context);
    }

    public OperationResult<User> login(String username, String password) {


         error =OperationResult.NO_ERROR;

        Firebase firebase = FirebaseClass.getFirebase();

        firebase.authWithPassword(
                username,
                password,
                new Firebase.AuthResultHandler() {
                    @Override
                    public void onAuthenticated(AuthData authData) {


                        Log.i("Error", "onAuthenticated: "+ authData.toString() );
                        Toast.makeText(mContext.getApplicationContext(), "Logou!!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onAuthenticationError(FirebaseError firebaseError) {
                        error = OperationResult.LOGIN_FAILED;
                        Toast.makeText(mContext.getApplicationContext(), "DEU ERRO!!", Toast.LENGTH_SHORT).show();
                    }
                }
        );




        OperationResult<User> result = new OperationResult<>();

        if(error != OperationResult.NO_ERROR) {
            result.setError(error);
        } else {
            result.setResult(new User("felipe","felipe"));
        }

        return result;
    }
}
