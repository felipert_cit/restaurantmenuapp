package com.cit.menurestaurant.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.cit.menurestaurant.R;
import com.cit.menurestaurant.entity.User;
import com.cit.menurestaurant.infrastructure.FirebaseClass;
import com.cit.menurestaurant.repository.database.Database;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.Map;

public class RegisterActivity extends Activity {

   private Button buttonCreateAccount;
   private EditText editTextName;
   private EditText editTextLogin;
   private EditText editTextPassword;
   private EditText editTextAge;
   private RadioGroup radioGroupGender;
   private RadioButton radioGender;
   private Spinner spinnerState;
   private CheckBox checkBoxNewsLetter;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        buttonCreateAccount = (Button) findViewById(R.id.button_register);

        buttonCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUser();
            }
        });

        editTextName = (EditText) findViewById(R.id.text_name);
        editTextLogin = (EditText) findViewById(R.id.login_text_login);
        editTextPassword = (EditText) findViewById(R.id.login_text_password);
        editTextAge = (EditText) findViewById(R.id.text_age);
        radioGroupGender = (RadioGroup) findViewById(R.id.radio_group_gender);
        radioGender= (RadioButton) findViewById(radioGroupGender.getCheckedRadioButtonId());
        checkBoxNewsLetter = (CheckBox) findViewById(R.id.check_news);
        spinnerState = (Spinner) findViewById(R.id.spinner_state);
    }

    private void createUser() {
        user = loadEntity();

        Firebase firebase = FirebaseClass.getFirebase();

        firebase.createUser(
                user.getEmail(),
                user.getPassword(),
                new Firebase.ValueResultHandler<Map<String, Object>>() {
                    @Override
                    public void onSuccess(Map<String, Object> stringObjectMap) {

                        user.setID(stringObjectMap.get("uid").toString());

                        Firebase firebase = FirebaseClass.getFirebase();
                        firebase = firebase.child("users").child(user.getID());

                        user.setPassword(null);
                        firebase.setValue(user);

                        firebase.unauth();

                        Toast.makeText(getApplicationContext(), "Conta criada com sucesso", Toast.LENGTH_LONG).show();

                        startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                    }

                    @Override
                    public void onError(FirebaseError firebaseError) {
                        Toast.makeText(getApplicationContext(), firebaseError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    private User loadEntity() {
        User user = new User();
        user.setName(editTextName.getText().toString());
        user.setEmail(editTextLogin.getText().toString());
        user.setPassword(editTextPassword.getText().toString());
        user.setAge(Integer.parseInt(editTextAge.getText().toString()));
        user.setGender(radioGender.getText().toString());
        user.setNews(checkBoxNewsLetter.isChecked());
        user.setState(spinnerState.getSelectedItem().toString());
        return user;
    }



}
