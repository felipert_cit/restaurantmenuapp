package com.cit.menurestaurant.repository.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.cit.menurestaurant.BuildConfig;
import com.cit.menurestaurant.repository.database.table.UserTable;

public class UserContentProvider extends ContentProvider {

    private static final String AUTHORITY = BuildConfig.APPLICATION_ID;
    private static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY);

    // Content URIs
    public static final Uri USER_CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, UserContent.CONTENT_PATH);

    // References of DIR and ID
    private static final int USER_DIR = 0;
    private static final int USER_LOGIN = 1;

    private static UriMatcher URI_MATCHER;

    // Configuration of UriMatcher
    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

        URI_MATCHER.addURI(AUTHORITY, UserContent.CONTENT_PATH, USER_DIR);
        URI_MATCHER.addURI(AUTHORITY, UserContent.CONTENT_PATH + "/#", USER_LOGIN);
    }

    private Database database;

    // Content Information of Entities

    /**
     * Instantiate the database, when the content provider is created.
     *
     * @return true
     */
    @Override
    public final boolean onCreate() {
        database = new Database(getContext());
        return true;
    }

    /**
     * Provide information whether uri returns an item or an directory.
     *
     * @param uri
     * @return content type from type Content.CONTENT_TYPE or Content.CONTENT_ITEM_TYPE
     */
    @Override
    public final String getType(Uri uri) {

        switch (URI_MATCHER.match(uri)) {

            case USER_DIR:
                return UserContent.CONTENT_TYPE;
            case USER_LOGIN:
                return UserContent.CONTENT_ITEM_TYPE;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

      /**
     * Execute a query on a given uri and return a Cursor with results.
     *
     * @param uri
     * @param projection
     * @param selection
     * @param selectionArgs
     * @param sortOrder
     * @return Cursor with results
     */
    @Override
    public final Cursor query(Uri uri, String[] projection, String selection,
                              String[] selectionArgs, String sortOrder) {

        SQLiteDatabase dbConnection = database.getReadableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        queryBuilder.setTables(UserTable.TABLE_NAME);

/*        switch (URI_MATCHER.match(uri)) {

            case USER_DIR:
                queryBuilder.setTables(UserTable.TABLE_NAME);
                break;
            case USER_LOGIN:
                queryBuilder.appendWhere(UserTable.KEY_LOGIN + "=" + uri.getPathSegments().get(1));
                break;


            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
*/
        Cursor cursor = queryBuilder.query(dbConnection, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }


    /**
     * Provides the content information of the ProductTable.
     */
    private static final class UserContent implements BaseColumns {

        /**
         * Specifies the content path of the ProductTable for the required Uri.
         */
        public static final String CONTENT_PATH = "product";

        /**
         * Sepecifies the type for the folder and the single item of the ProductTable.
         */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.mdsdacp.user";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.mdsdacp.user";
    }

}
