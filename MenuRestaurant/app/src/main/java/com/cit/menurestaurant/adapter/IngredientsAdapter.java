package com.cit.menurestaurant.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cit.menurestaurant.R;
import com.cit.menurestaurant.entity.Ingredient;
import com.cit.menurestaurant.ui.IngredientActivity;

import java.util.List;

/**
 * Created by fetav on 14/03/2016.
 */
public class IngredientsAdapter extends BaseAdapter {

    private Context context;
    private AppCompatActivity activity;

    private List<Ingredient> ingredients;

    public IngredientsAdapter(AppCompatActivity activity, List<Ingredient> ingredients) {
        this.ingredients = ingredients;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return ingredients.size();
    }

    @Override
    public Object getItem(int position) {
        return ingredients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ingredients.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();

        View row;

        row = inflater.inflate(R.layout.ingredient_item_layout, parent, false);

        TextView title, detail,value;

        title = (TextView) row.findViewById(R.id.ingredient_item_title);
        detail = (TextView) row.findViewById(R.id.ingredient_item_descr);
        value = (TextView) row.findViewById(R.id.ingredient_item_value);

        title.setText(ingredients.get(position).getName());
        detail.setText(ingredients.get(position).getDescription());
        value.setText(ingredients.get(position).getQuantity());

        return (row);
    }
}
