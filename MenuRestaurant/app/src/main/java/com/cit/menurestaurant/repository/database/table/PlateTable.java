package com.cit.menurestaurant.repository.database.table;

/**
 * Created by felipert on 15/02/2016.
 */
public interface PlateTable {
    String TABLE_NAME = "plates";

    String KEY_ID = "_id";
    String KEY_NAME = "name";
    String KEY_DESCRIPTION = "description";
    String KEY_VALUE = "value";


    String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + KEY_ID + " INT PRIMARY KEY,"
            + KEY_NAME + " TEXT,"
            + KEY_DESCRIPTION + " TEXT KEY,"
            + KEY_VALUE + " REAL "
            + ")";

    String INSERT_TABLE = "INSERT INTO " + TABLE_NAME + "("
            + KEY_NAME + ","
            + KEY_DESCRIPTION + ","
            + KEY_VALUE + ") VALUES ";



}
