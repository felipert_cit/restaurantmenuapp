package com.cit.menurestaurant.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cit.menurestaurant.R;
import com.cit.menurestaurant.entity.Meal;
import com.cit.menurestaurant.entity.Restaurant;
import com.cit.menurestaurant.infrastructure.FirebaseClass;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.MutableData;
import com.firebase.client.Transaction;
import com.firebase.client.ValueEventListener;
import com.firebase.ui.FirebaseListAdapter;

import java.io.ByteArrayOutputStream;

public class MenuActivity extends AppCompatActivity {

    private ListView listMenu;
    private  int idRestaurant;
    private String nameRestaurant;
    TextView txtNameRestaurant;
    Firebase ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        if(getIntent().hasExtra("restaurantID")){
            Toast.makeText(MenuActivity.this,String.valueOf( getIntent().getExtras().getInt("restaurantID")), Toast.LENGTH_LONG).show();
            idRestaurant = getIntent().getExtras().getInt("restaurantID");
            txtNameRestaurant = (TextView) findViewById(R.id.menu_label_name);
            listMenu = (ListView) findViewById(R.id.menu_list_food);



            ref = FirebaseClass.getFirebase().child("restaurants").child(String.valueOf(idRestaurant));


            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    nameRestaurant = dataSnapshot.getValue(Restaurant.class).getName();
                    txtNameRestaurant.setText("Restaurante " + nameRestaurant);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });


        FirebaseListAdapter<Meal> mAdapter = new FirebaseListAdapter<Meal>(MenuActivity.this, Meal.class, R.layout.layout_item_restaurant, ref.child("menu")) {
            @Override
            protected void populateView(View view, Meal meal) {
                TextView text = (TextView) view.findViewById(R.id.item_name);
                text.setText(meal.getName());

                view.setId(meal.getId());

                text = (TextView) view.findViewById(R.id.item_description);
                text.setText(meal.getDescription());

                Log.i("bindList",meal.toString());

                if(meal.getImage()!=null) {
                    ImageView image = (ImageView) view.findViewById(R.id.item_image);

                    byte[] decodedString = Base64.decode(meal.getImage(), Base64.DEFAULT);

                   final Bitmap decoded = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                    image.setImageBitmap(decoded);

                    image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog nagDialog = new Dialog(MenuActivity.this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                            nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            nagDialog.setCancelable(false);
                            nagDialog.setContentView(R.layout.preview_image);
                            ImageView ivPreview = (ImageView) nagDialog.findViewById(R.id.iv_preview_image);
                            ivPreview.setImageBitmap(decoded);

                            ivPreview.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View arg0) {

                                    nagDialog.dismiss();
                                }
                            });

                            nagDialog.show();
                        }
                    });
                }
            }


        };
            listMenu.setAdapter(mAdapter);

            listMenu.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(MenuActivity.this, "long click", Toast.LENGTH_SHORT).show();
                    Intent intentAvalia = new Intent(MenuActivity.this, AvaliateActivity.class);

                    Bundle bundle = new Bundle();

                    bundle.putInt("restaurantID", idRestaurant);
                    bundle.putInt("menuID", view.getId());
                    bundle.putString("nameRestaurant", nameRestaurant);
                    intentAvalia.putExtras(bundle);

                    startActivity(intentAvalia);

                    return true;
                }
            });


            listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(MenuActivity.this, "idRestaurant= " + idRestaurant + " idMenu" + view.getId(), Toast.LENGTH_SHORT).show();

                    Intent intentMenu = new Intent(MenuActivity.this, IngredientActivity.class);

                    Bundle bundle = new Bundle();

                    bundle.putInt("restaurantID", idRestaurant);
                    bundle.putInt("menuID", view.getId());
                    bundle.putString("nameRestaurant", nameRestaurant);
                    intentMenu.putExtras(bundle);

                    startActivity(intentMenu);

                }
            });

        }
    }
}
