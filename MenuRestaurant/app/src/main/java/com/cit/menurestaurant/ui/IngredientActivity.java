package com.cit.menurestaurant.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cit.menurestaurant.R;
import com.cit.menurestaurant.adapter.IngredientsAdapter;
import com.cit.menurestaurant.entity.Ingredient;
import com.cit.menurestaurant.entity.Meal;
import com.cit.menurestaurant.infrastructure.FirebaseClass;
import com.cit.menurestaurant.repository.database.Database;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.MutableData;
import com.firebase.client.Transaction;
import com.firebase.client.ValueEventListener;
import com.firebase.ui.FirebaseListAdapter;

import java.util.ArrayList;
import java.util.List;

public class IngredientActivity extends AppCompatActivity {

    int idRestaurant;
    int idMenu;
    String nameRestaurant;
    Firebase ref;
    TextView textTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredient);

        if(getIntent().hasExtra("restaurantID")){
            Toast.makeText(this, String.valueOf(getIntent().getExtras().getInt("restaurantID")), Toast.LENGTH_LONG).show();
            idRestaurant = getIntent().getExtras().getInt("restaurantID");
            nameRestaurant = getIntent().getExtras().getString("nameRestaurant");
            idMenu = getIntent().getExtras().getInt("menuID");

            textTitle = (TextView) findViewById(R.id.ingredient_title);

            ListView list = (ListView) findViewById(R.id.ingredient_list);

            ref = FirebaseClass.getFirebase()
                    .child("restaurants")
                    .child(String.valueOf(idRestaurant))
                    .child("menu")
                    .child(String.valueOf(idMenu));

            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    textTitle.setText(dataSnapshot.getValue(Meal.class).getName());
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

            FirebaseListAdapter<Ingredient> mAdapter = new FirebaseListAdapter<Ingredient>(this, Ingredient.class, R.layout.ingredient_item_layout, ref.child("ingredients")) {
                @Override
                protected void populateView(View view, Ingredient ingredient) {
                    TextView text = (TextView) view.findViewById(R.id.ingredient_item_title);
                    text.setText(ingredient.getName());

                    view.setId(ingredient.getId());

                    text = (TextView) view.findViewById(R.id.ingredient_item_descr);
                    text.setText(ingredient.getDescription());

                    text = (TextView) view.findViewById(R.id.ingredient_item_value);
                    text.setText(ingredient.getQuantity());
                }
            };
            list.setAdapter(mAdapter);
        }

    }
}
