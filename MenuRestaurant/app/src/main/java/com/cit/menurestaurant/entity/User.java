package com.cit.menurestaurant.entity;

/**
 * Created by felipert on 15/02/2016.
 */
public class User {

    private String ID;
    private String _name;
    private String _email;
    private String _password;
    private int _age;
    private String _gender;
    private String _state;
    private boolean _news;

    public User(String _name, String _email) {
        this._name = _name;
        this._email = _email;
    }

    public User(){}

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return _name;
    }

    public void setName(String _name) {
        this._name = _name;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String _email) {
        this._email = _email;
    }

    public String getPassword() {
        return _password;
    }

    public void setPassword(String _password) {
        this._password = _password;
    }

    public int getAge() {
        return _age;
    }

    public void setAge(int _age) {
        this._age = _age;
    }

    public String getGender() {
        return _gender;
    }

    public void setGender(String _gender) {
        this._gender = _gender;
    }

    public String getState() {
        return _state;
    }

    public void setState(String _state) {
        this._state = _state;
    }

    public boolean isNews() {
        return _news;
    }

    public void setNews(boolean _news) {
        this._news = _news;
    }

    @Override
    public String toString() {
        return "User{" +
                "_name='" + _name + '\'' +
                ", _email='" + _email + '\'' +
                ", _password='" + _password + '\'' +
                ", _age=" + _age +
                ", _gender='" + _gender + '\'' +
                ", _state='" + _state + '\'' +
                ", _news=" + _news +
                "}\n";
    }
}
