package com.cit.menurestaurant.repository.database.table;

/**
 * Created by felipert on 15/02/2016.
 */
public interface IngredientTable {
    String TABLE_NAME = "ingredients";

    String KEY_ID = "_id";
    String KEY_NAME = "name";
    String KEY_DESCRIPTION = "description";


    String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + KEY_ID + " INT PRIMARY KEY,"
            + KEY_NAME + " TEXT,"
            + KEY_DESCRIPTION + " TEXT )";

    String INSERT_TABLE = "INSERT INTO " + TABLE_NAME + "("
            + KEY_NAME + ","
            + KEY_DESCRIPTION + ") VALUES ";

    String SELECT = "SELECT "
            +KEY_ID+","
            +KEY_NAME+","
            +KEY_DESCRIPTION
            +" FROM " + TABLE_NAME;



}
