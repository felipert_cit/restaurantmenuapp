package com.cit.menurestaurant.entity;


import java.util.LinkedList;
import java.util.List;

public class Restaurant {

    private int id;
    private String name;
    private String description;
    private List<Meal> menu;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Meal> getMenu() {
        if(this.menu==null)
            this.menu = new LinkedList<Meal>();

        return menu;
    }

    public void setMenu(List<Meal> menu) {
        this.menu = menu;
    }

    public void addMenu(Meal f){

        if(this.menu==null)
            this.menu = new LinkedList<Meal>();

        this.menu.add(f);

    }

}
