package com.cit.menurestaurant.business;

import android.content.Context;

public class BaseBusiness {

    protected Context mContext;

    protected BaseBusiness(Context context) {
        mContext = context;
    }
}
