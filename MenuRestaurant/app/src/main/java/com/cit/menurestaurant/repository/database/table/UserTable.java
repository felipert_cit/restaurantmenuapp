package com.cit.menurestaurant.repository.database.table;

/**
 * Created by felipert on 15/02/2016.
 */
public interface UserTable {
    String TABLE_NAME = "users";

    String KEY_ID = "_id";
    String KEY_NAME = "name";
    String KEY_LOGIN = "login";
    String KEY_PASSWORD = "password";
    String KEY_AGE = "age";
    String KEY_GENDER = "gender";
    String KEY_STATE = "state";
    String KEY_NEWS = "news";

    String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + KEY_ID + " INT PRIMARY KEY,"
            + KEY_LOGIN + " TEXT KEY,"
            + KEY_NAME + " TEXT,"
            + KEY_PASSWORD + " TEXT,"
            + KEY_AGE + " INTEGER,"
            + KEY_GENDER + " TEXT,"
            + KEY_STATE + " TEXT,"
            + KEY_NEWS + " INTEGER "
            + ")";

   String SELECT = "SELECT "
            +KEY_NAME+","
            +KEY_LOGIN+","
            +KEY_PASSWORD+","
            +KEY_AGE+","
            +KEY_GENDER+","
            +KEY_STATE+","
            +KEY_NEWS
            +" FROM " + TABLE_NAME;
}
