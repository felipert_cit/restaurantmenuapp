package com.cit.menurestaurant.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cit.menurestaurant.R;
import com.cit.menurestaurant.business.UserBusiness;
import com.cit.menurestaurant.entity.Restaurant;
import com.cit.menurestaurant.infrastructure.FirebaseClass;
import com.cit.menurestaurant.manager.UserManager;
import com.firebase.client.Firebase;
import com.firebase.ui.FirebaseListAdapter;

public class RestaurantActivity extends AppCompatActivity {

    ListView listRestaurant;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        listRestaurant = (ListView) findViewById(R.id.restaurant_list);

        Firebase ref = FirebaseClass.getFirebase();

        TextView txtTitle = (TextView) findViewById(R.id.restaurant_title);
        Button btnLogout = (Button) findViewById(R.id.restaurant_button_logout);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RestaurantActivity.this,LoginActivity.class);

                intent.putExtra("logout",true);

                startActivity(intent);
            }
        });


        mSharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);
        String userName =mSharedPreferences.getString(UserBusiness.KEY_USER_NAME,null);
        if (  userName!=null){

            txtTitle.setText("Bem vindo "+userName);
        }

        ref = ref.child("restaurants");

        FirebaseListAdapter<Restaurant> mAdapter = new FirebaseListAdapter<Restaurant>(this, Restaurant.class, R.layout.layout_item_restaurant, ref) {
            @Override
            protected void populateView(View view, Restaurant object) {

                TextView text = (TextView) view.findViewById(R.id.item_name);
                text.setText(object.getName());

                view.setId(object.getId());

                text = (TextView) view.findViewById(R.id.item_description);
                text.setText(object.getDescription());

            }

        };

        listRestaurant.setAdapter(mAdapter);

        listRestaurant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(RestaurantActivity.this, "id= " + view.getId(), Toast.LENGTH_SHORT).show();

                Intent intentMenu = new Intent(RestaurantActivity.this, MenuActivity.class);

                Bundle bundle = new Bundle();

                bundle.putInt("restaurantID", view.getId());
                intentMenu.putExtras(bundle);

                startActivity(intentMenu);

            }
        });

    }
}
