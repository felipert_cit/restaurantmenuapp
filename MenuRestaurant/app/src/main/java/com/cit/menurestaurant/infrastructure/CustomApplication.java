package com.cit.menurestaurant.infrastructure;

import android.app.Application;

import com.firebase.client.Firebase;

/**
 * Created by fetav on 23/03/2016.
 */
public class CustomApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
    }
}
