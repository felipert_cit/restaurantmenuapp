package com.cit.menurestaurant.repository.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;

import com.cit.menurestaurant.BuildConfig;
import com.cit.menurestaurant.repository.database.table.PlateTable;


public class PlateContentProvider extends ContentProvider {

    private static final String AUTHORITY = BuildConfig.APPLICATION_ID;
    private static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY + "_plate");

    // References of DIR and ID
    private static final int PLATE_DIR = 0;


    private static UriMatcher URI_MATCHER;

    // Configuration of UriMatcher
    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

        URI_MATCHER.addURI(AUTHORITY, UserContent.CONTENT_PATH, PLATE_DIR);
     }

    private Database database;

    // Content Information of Entities

    /**
     * Instantiate the database, when the content provider is created.
     *
     * @return true
     */
    @Override
    public final boolean onCreate() {
        database = new Database(getContext());
        return true;
    }

    /**
     * Provide information whether uri returns an item or an directory.
     *
     * @param uri
     * @return content type from type Content.CONTENT_TYPE or Content.CONTENT_ITEM_TYPE
     */
    @Override
    public final String getType(Uri uri) {

        switch (URI_MATCHER.match(uri)) {

            case PLATE_DIR:
                return UserContent.CONTENT_TYPE;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

      /**
     * Execute a query on a given uri and return a Cursor with results.
     *
     * @param uri
     * @param projection
     * @param selection
     * @param selectionArgs
     * @param sortOrder
     * @return Cursor with results
     */
    @Override
    public final Cursor query(Uri uri, String[] projection, String selection,
                              String[] selectionArgs, String sortOrder) {

        SQLiteDatabase dbConnection = database.getReadableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        queryBuilder.setTables(PlateTable.TABLE_NAME);


        Cursor cursor = queryBuilder.query(dbConnection, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }


    /**
     * Provides the content information of the ProductTable.
     */
    private static final class UserContent implements BaseColumns {

        /**
         * Specifies the content path of the ProductTable for the required Uri.
         */
        public static final String CONTENT_PATH = "plate";

        /**
         * Sepecifies the type for the folder and the single item of the ProductTable.
         */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.mdsdacp.user";
    }

}
