package com.cit.menurestaurant.manager;

import android.content.Context;
import android.os.AsyncTask;

import com.cit.menurestaurant.business.UserBusiness;
import com.cit.menurestaurant.entity.User;
import com.cit.menurestaurant.infrastructure.OperationListener;
import com.cit.menurestaurant.infrastructure.OperationResult;

/**
 * Created by felipert on 15/02/2016.
 */
public class UserManager extends BaseManager {

    private UserBusiness mUserBusiness;

    public UserManager(Context context) {
        super(context);
        mUserBusiness = new UserBusiness(mContext);
    }

    public void login(final String userName, final String password, final OperationListener<User> listener) {
        AsyncTask<Void, Integer, OperationResult<User>> task = new AsyncTask<Void, Integer, OperationResult<User>>() {

            @Override
            protected OperationResult<User> doInBackground(Void... params) {
                // TODO: implements the logic for deciding where to fetch the data from (backend or database)

                return mUserBusiness.login(userName, password);
            }

            @Override
            protected void onPostExecute(OperationResult<User> operationResult) {
                removeFromTaskList(this);
                if (listener != null) {
                    int error = operationResult.getError();
                    if (error != OperationResult.NO_ERROR) {
                        listener.onError(error);
                    } else {
                        listener.onSuccess(operationResult.getResult());
                    }
                }
            }

            @Override
            protected void onCancelled() {
                removeFromTaskList(this);
                if (listener != null) {
                    listener.onCancel();
                }
            }
        };

        // Task execution
        addToTaskList(task);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}