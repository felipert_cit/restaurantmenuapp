package com.cit.menurestaurant.repository.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.cit.menurestaurant.entity.Ingredient;
import com.cit.menurestaurant.entity.User;
import com.cit.menurestaurant.repository.database.table.IngredientTable;
import com.cit.menurestaurant.repository.database.table.PlateTable;
import com.cit.menurestaurant.repository.database.table.UserTable;

import java.net.UnknownServiceException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by felipert on 15/02/2016.
 */
public class Database extends SQLiteOpenHelper {

    // versão do banco é usada quando for atualizar o banco.
    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_NAME = "Restaurant.db";


    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        db.execSQL(UserTable.CREATE_TABLE);

        db.execSQL(PlateTable.CREATE_TABLE);

        db.execSQL(IngredientTable.CREATE_TABLE);

        db.execSQL(PlateTable.INSERT_TABLE + " (\"DO DIA\",\" ARROZ E FEIJÃO\",10) ");

        db.execSQL(PlateTable.INSERT_TABLE + " (\"Macarronada\",\" macarrão\",20) ");

        db.execSQL(PlateTable.INSERT_TABLE + " (\"estrogonófe\",\" de flango\",100) ");

        db.execSQL(IngredientTable.INSERT_TABLE + " (\"arroz\",\" =D\") ");
        db.execSQL(IngredientTable.INSERT_TABLE + " (\"flango\",\" de galinha\") ");
        db.execSQL(IngredientTable.INSERT_TABLE + " (\"fejãoo\",\" broto legao\") ");



    }


    /**
     * Metodo é automaticamente usado quando vc troca a versão do banco,
     * aque vc deve aplicar as mudanças previstas para tudo continuar funcionando
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + UserTable.TABLE_NAME);
        onCreate(db);
    }

    /**
     * Método usado para adicionar um contato no banco, e retornar o id que foi gerado para este contato
     * @param user
     * @return id
     */
    public int addUser(User user) {

        // o banco esta encapsulado nesta classe
        // existem algumas formas de acessar o banco, neste caso, como vamos alterar,
        // usamos o getWritableDatabase
        SQLiteDatabase db = this.getWritableDatabase();

        // ContentValues é um HashMap, ele vai mapear a Key=nome da coluna
        // para o value=valor que vai ser inserido
        ContentValues values = new ContentValues();
        values.put(UserTable.KEY_NAME, user.getName());
        values.put(UserTable.KEY_LOGIN, user.getEmail());
        values.put(UserTable.KEY_PASSWORD, user.getPassword());
        values.put(UserTable.KEY_AGE, user.getAge());
        values.put(UserTable.KEY_GENDER, user.getGender());
        values.put(UserTable.KEY_STATE, user.getState());
        values.put(UserTable.KEY_NEWS, user.isNews()? 1:0 );

        db.insert(UserTable.TABLE_NAME, null, values);
        db.close();

        return getUsersCount();
    }

    public List<User> getAllUsers() {

        List<User> usersList = new ArrayList<User>();

        String selectQuery = "SELECT "
                              +UserTable.KEY_NAME+","
                              +UserTable.KEY_LOGIN+","
                              +UserTable.KEY_PASSWORD+","
                              +UserTable.KEY_AGE+","
                              +UserTable.KEY_GENDER+","
                              +UserTable.KEY_STATE+","
                              +UserTable.KEY_NEWS
                              +" FROM " + UserTable.TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // movetoFirst vai retornar falso se não encontrar nenhum registro
        if (cursor.moveToFirst()) {
            do {

                User user = new User("a","a");

                // o cursor é uma linha, usamos o getString e passamos o indice da coluna que
                // queremos pegar a informação

                user.setName(cursor.getString(cursor.getColumnIndex(UserTable.KEY_NAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(UserTable.KEY_LOGIN)));
                user.setPassword(cursor.getString(cursor.getColumnIndex(UserTable.KEY_PASSWORD)));
                user.setAge(cursor.getInt(cursor.getColumnIndex(UserTable.KEY_AGE)));
                user.setGender(cursor.getString(cursor.getColumnIndex(UserTable.KEY_GENDER)));
                user.setState(cursor.getString(cursor.getColumnIndex(UserTable.KEY_STATE)));
                user.setNews((cursor.getInt(cursor.getColumnIndex(UserTable.KEY_NEWS))==1) );
                usersList.add(user);

            } while (cursor.moveToNext());
        }
        db.close();

        return usersList;
    }
    /**
     * Metodo retorna a quantidade de registros do banco de dados
     * @return
     */
    public int getUsersCount() {
        String countQuery = "SELECT  * FROM " + UserTable.TABLE_NAME;
        // neste caso, como só vamos ler,  usamos o metodo getReadableDatabase
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    public List<Ingredient> getAllIngredients() {

        List<Ingredient> ingredientList = new ArrayList<Ingredient>();

        String selectQuery = IngredientTable.SELECT;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // movetoFirst vai retornar falso se não encontrar nenhum registro
        if (cursor.moveToFirst()) {
            do {

                Ingredient ingredient = new Ingredient();

                // o cursor é uma linha, usamos o getString e passamos o indice da coluna que
                // queremos pegar a informação

                ingredient.setId(cursor.getInt(cursor.getColumnIndex(IngredientTable.KEY_ID)));
                ingredient.setName(cursor.getString(cursor.getColumnIndex(IngredientTable.KEY_NAME)));
                ingredient.setDescription(cursor.getString(cursor.getColumnIndex(IngredientTable.KEY_DESCRIPTION)));

                ingredientList.add(ingredient);

            } while (cursor.moveToNext());
        }
        db.close();

        return ingredientList;
    }

}